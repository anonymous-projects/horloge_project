# HORLOGE PROJECT #

Ce projet a pour but d'afficher l'heure selon les différentes timezone et pouvoir paramétrer des alarmes n'importe où dans le monde.

Projet crée sous linux, toutes les indications seront donc basées sur linux

## Installation ###

* Ce projet requiert python3 / pip / Node >= v12.13.1 / npm

### ReactJs / NodeJs ## 

* Cloner le projet puis installer les node_modules via le package.json
* *React :* `cd <votre repertoire>/frontend && npm install`
* *Node :* `cd <votre repertoire>/backend/nodejs && npm install`

### Python ##

* Utiliser un environnement virtuel pour installer les dépendances python dans un endroit cloisonné, ici nous utiliserons virtualenv (https://virtualenv.pypa.io/en/latest/)
* Commencer par créer votre environnement `cd <votre repertoire>/backend/python && virtualenv myenv`
* Initialisation de l'environnement `source myenv/bin/activate`
* Installation des dépendances `pip install -r requirements.txt`

## Utilisation ###

#### Paramétrer vos variables d'environnement: #####

* Les variables utilisées par React par default:
	* `REACT_APP_SOCKETADDR=localhost` *(l'adresse ip du serveur socket)*
	* `REACT_APP_SOCKETPORT=5020` *(le port du serveur socket)*
	* `PORT=3000` *(le port d'ecoute du serveur react)*
* Les variables utilisées par Node / Python par default:
	* `PORT=5020` *(Le port d’écoute du serveur)*
	* `UTC_OFFSET=1` *(Ajoute ou retire une heure)*

##### Modification 
* Pour Node et React créer a la racine  du répertoire `frontend` ou `backend/nodejs` un ficher appelé .env pour les modifier 
* Pour python faite un export dans l'environnement classique `export VAR=value`

#### Lancement des serveurs 
* Pour python initialisé au préalable votre virtualenv :
	* `source backend/python/<myenv>/bin/activate` 
* Aller la racine du projet puis choisissez le serveur à lancer :
	* `sh launchers/<server>.sh`
* Browser: `<ip du serveur react>:<port du serveur react>`
	* localhost:3000 *(par default)*

## Ameliorations Techniques ###

* Ecriture des unittests pour React / Node / Python
* Lancer les serveurs avec des outils de production comme gunicorn pour python, puis un utiliser un server / reverse proxy comme nginx pour utiliser des DNS et pouvoir y accéder via le port 80 et installer des certificats SSL pour une connexion sécuriser
* Dockeriser l'ensemble ainsi que des pipelines CI/CD

#### BackEnd
* Utiliser une base donnée pour stocker les utilisateurs virtuels
* Utiliser des outils de validation de dictionnaires comme marshmallow / validate.js
* Si le site devient important utiliser un broker comme rabbitMQ pour gérer de façon plus fluide l'affluence des requêtes.
* Améliorer les exceptions en gérant les cas spécifiques avec d'autres cas que `Exception`
* En NodeJS trouver le moyen de récupérer l'heure des différentes timezones avec un timestamp pour éviter de toujours faire la conversion en datetime pour les comparer.
* Créer un logger en NodeJS et une structure de dossier

#### FrontEnd
* Créer un ID unique avec un timestamp + adresseIP + randomNumber pour pouvoir récupérer ses alarmes et utiliser les cookies ou le local storage pour stocker les alarmes et/ou l'ID unique pour ainsi pouvoir les retrouver si le serveur se déconnecte et réémettre les alarmes créées.
* Trier les timezones et pouvoir les chercher via un input
* Mettre a jour la date de l'alarme lors d'un changement de timezone.

## Axes d’évolutions du Projet ##

* L'utilisateur doit pouvoir créer un profil / s'authentifier et retrouver ses alarmes
* Afficher plusieurs timezones / different format d'affichage de l'heure
* Ajouter des messages personnaliser sur les alarmes
* Créer des snooz sur les alarmes
* Avertir l'utilisateur par email / Notification
* Afficher toutes alarmes via un bouton / tout supprimer 
* Afficher d'autres informations sur les timezones (Population, News)