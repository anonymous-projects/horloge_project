const io = require('socket.io')();
const fs = require('fs');
const moment = require('moment-timezone');
const dotenv = require('dotenv');
	  dotenv.config();

let   project_env = {}
let   timezones = []
let   alarmes = []

const process_env = process.env
const default_env = {
	'PORT':[parseInt, 5020],
	'UTC_OFFSET':[parseInt, 1]
}

for (const [key, dflt] of Object.entries(default_env)) {
 	project_env[key] = process_env.hasOwnProperty(key) ? dflt[0](process_env[key]) : dflt[1]
}

console.log('ENV', project_env)

io.listen(project_env['PORT']);

/**
 * Recupere les timezones à dispatcher depuis un fichier json
 */

const get_timezone_JSON = () => {

	let _timezones

	try {
		let timezonesJSON 	= fs.readFileSync('timezones.json');
		_timezones 			= JSON.parse(timezonesJSON);
	} catch (err) {
	  console.log(err.message)
	  return false
	}

	return _timezones
}

const set_timezones = () => {

	const _timezones =  get_timezone_JSON()

	if (_timezones == false) {
		return false
	}

	for (const continent of _timezones) {
	 	for (const zone of continent['zones']) {
			timezones.push(zone.value)
		}
	}

	return true
}

/**
 * Retourne true si l'alarme passé en parametre est incluses dans l'objet alarmes, false sinon
 * @param  {Object} curr_alarme Objet a verifier.
 * @return {Boolean} correspond a l'inclusion dans l'objet alarme
 */
function includeAlarme(curr_alarme) {

	for (const alarme of alarmes) {
        if (alarme['sid'] == curr_alarme['sid'] && alarme['tz'] == curr_alarme['tz'] && alarme['timestamp'] == curr_alarme['timestamp']) {
            return true;
        }
    }
    return false;
}

/**
 * Retourne l'heure actuelle en fonction d'une timezone
 * @param  {String} timezone la timezone a appliquer a la date actuelle.
 * @return {String} L'heure actuelle au format YYYY-MM-DDTHH:mm:ss
 */
const get_current_timestamp = (timezone) => {
	return moment.tz(moment(), timezone).format('YYYY-MM-DDTHH:mm:ss')
	// return moment.tz(new Date().toISOString().split('.')[0], "YYYY-MM-DDTHH:mm:ss", timezone).valueOf();
}


/**
 * Emet l'heure actuelle de facon asynchrone en fonction d'une timezone.
 * @param  {timezone} timezone la timezone a appliquer a la date actuelle.
 * @return {Promise}  Promise.reject si le traitement a echouer, sinon undefined
 */
const emitAsync = async timezone => {

	try {

		const 	_currts = get_current_timestamp(timezone)

		io.to(timezone).emit('timestamp', _currts)
		
		const 	currts  = new Date(_currts)
		const 	currts_stop  = new Date(_currts)
				currts_stop.setSeconds( currts_stop.getSeconds() + 1 );

		for (let i = alarmes.length - 1; i >= 0; i--) {
		    

		    const {tz, timestamp, sid} = alarmes[i]
		    const alarmts = new Date(timestamp)

		    if (alarmts <= currts && timezone == tz){
		    	io.to(sid).emit('alarmRing', alarmes[i])
		    	alarmes.splice(i, 1);
		    }
		    else if (alarmts > currts_stop) {
		    	break
		    }
		}
	}
	catch (e) {
		return Promise.reject(e);
	}
}
/**
 * Timer infinie permettant de dispatcher les timezones a chaque seconde sur les differentes rooms de facon asynchrone
 */
const run_loop = () => {

	setInterval(() => {

		Promise.all(timezones.map(timezone => emitAsync(timezone)))
		.then(
			data => {},
			error => {
				console.log(`An error occured while map async on alarmes - ${error}`)
			}
		)

	}, 1000);

}

/**
 * La connection a la socket et ses events
 */
io.on('connect', function (socket) {
	
	socket.join(socket.id);
	console.log('new client connected', socket.id)

	socket.on('disconnect', () => {
		const sid = socket.sid
		alarmes = alarmes.filter(alarme => alarme.sid != sid)
	    console.log('client disconnect', socket.id);
	});

	socket.on('join', (data) => {
		try{
			socket.join(data.room);
			socket.emit('timestamp', get_current_timestamp(data.room));
		    console.log('client want to join room', socket.id, data.room);
		}catch (err) {
		  console.log(err.message)
		}
	});

	socket.on('leave', (data) => {
		try{
			socket.leave(data.room);
		    console.log('client want to leave room', socket.id, data.room);
		}catch (err) {
		  console.log(err.message)
		}
	});

	socket.on('timezoneData',() => {

	    console.log('client wants to get timezones', socket.id);

		const _timezones =  get_timezone_JSON()

		if (_timezones == false) {
			return false
		}

		socket.emit('timezoneData', _timezones);
	    
	});

	socket.on('set_alarme', (data) => {

		try{
		    if (data.constructor == Object && data.hasOwnProperty('tz') && data.hasOwnProperty('timestamp') && timezones.includes(data['tz']) && typeof data['timestamp'] == 'number' ){

				_currts  = get_current_timestamp(data['tz'])
				_alarmts =  moment.unix(data['timestamp']).utcOffset(project_env['UTC_OFFSET']).format('YYYY-MM-DDTHH:mm:ss')

				if ( new Date(_alarmts) < new Date(_currts) ){
					socket.emit('alarme_set', {'success': false, 'msg':'wrong date / informations sent to server'})
					return false
				}

				data['sid'] = socket.id
				data['timestamp'] = _alarmts

				if (includeAlarme(data) == false){
					alarmes.push(data)
					alarmes.sort((a,b) => {return new Date(b.timestamp) - new Date(a.timestamp)})
					socket.emit('alarme_set', {success: true, data})
					console.log('New alarm has been set', data)
				}
			}
		} catch (err) {
		  console.log(err.message)
		}
	});
	
	socket.on('remove_alarme', (data) => {
		try{
			alarmes = alarmes.filter(alarme => !(alarme.sid == socket.id && alarme.tz == data.tz && alarme.timestamp == data.timestamp))
			socket.emit('remove_alarme', {success: true, data})
		    console.log('Alarm has been removed', data);
		} catch (err) {
		  console.log(err.message)
		}
	});
});

const r = set_timezones()

if (r == false){
	io.close()
}
else{
	run_loop()
}
