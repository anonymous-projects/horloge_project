from tools 		import Mysql_client

import json

class create_table(object):
	

	def __init__(self):
		with open("params/db_infos.json") as dbinfo:
			_dbinfo = json.load(dbinfo)
			self.db = Mysql_client(_dbinfo)
	
	def create_talarmes(self):
		query = """
			CREATE TABLE IF NOT EXISTS alarmes(
				id INT primary key NOT NULL AUTO_INCREMENT,
			 	sid VARCHAR(100) NOT NULL,
			 	timestamp INT NOT NULL,
			 	tz VARCHAR(100),
			  	UNIQUE KEY(sid, timestamp, tz)
			);
			"""
		try:
			self.db.execute_query(query)
		except Exception as err:
			pass
		else:
			print("table customers exists or has been created successfully")

if __name__ == "__main__":
	CT = create_table()
	CT.create_talarmes()