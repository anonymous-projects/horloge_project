# -*- coding: utf-8 -*-

from flask 			import request
from flask_socketio import Namespace, emit, join_room, leave_room, disconnect
from operator		import itemgetter
from threading 		import Lock

import arrow, json, sqlite3
from datetime import datetime

import os

thread_lock = Lock()

class RootNS_mysql(Namespace):

	@classmethod
	def __initspec__(cls, manager):

		""" Initialisation speciale de la class RootNs pour pouvoir communiquer avec le parent """
		
		cls._manager 	= manager
		cls._db			= manager.db
		cls._socketio 	= manager.socketio
		cls._printer 	= manager.printer
		cls._table		= 'alarmes'
		cls._offset  	= os.environ.get('UTC_OFFSET', -1)
		return cls

	def _raise_on_error(self, msg="Something goes wrong"):
		raise Exception(msg)

	def _emit(self, data={}, event='message', room=None):

		""" Emets un message a client, une room, un event selon les parametres 
			
			param data: les message a envoyer
			type data: any, optional
			param event: l'event de transimission
			type event: str, optional
			param room: la room de transmission
			type room: str, optional

		"""
		try:
			self._socketio.emit(event, data, room=room, namespace='/')
		except Exception as err:
			self._printer('An error occured while emiting socketio - {}'.format(err))

	def _get_timestamp_by_timezone(self, timezone):
			return arrow.now(timezone).shift(hours=self._offset).replace(tzinfo='UTC').timestamp

	def _check_and_remove(self, _currts, room):
		""" Verifier dans la bdd les alarmes actuelle et les supprime a la fin"""
		try:
			_query = "SELECT * FROM {} WHERE timestamp<={} and tz='{}'".format(self._table, _currts, room)
			_alarmes = self._db.execute_query(_query, fetch='all')
			_has_ring = []
			for alarm in _alarmes:
				_alarmtz = alarm.get('tz')
				_alarmts = alarm.get('timestamp')
				_sid	 = alarm.get('sid')
				_has_ring.append( (_alarmtz, _alarmts, _sid) )
				self._emit(event='alarmRing', data=alarm, room=_sid)
			_query = "DELETE FROM {} WHERE tz=%s AND timestamp=%s AND sid=%s".format(self._table)
			self._db.execute_many_query(_query, _has_ring)
		except Exception as err:
			self._printer('An error occured on _check_and_remove - {}'.format(err))

	def _emit_timestamp(self, room):

		""" Callback permettant d'emettre le timestamp actuelle selon une timezone a la room correspondant a ce timezone 
			param room: la room de transimission
			type room: str
		"""

		try:

			_currts = self._get_timestamp_by_timezone(room)
			
			self._emit(event='timestamp', data=_currts, room=room)
			self._manager.pool.spawn_n(self._check_and_remove, _currts, room)

		except Exception as err:
			self._printer('An error occured on _emit_timestamp - {}'.format(err))


	def _sort_by_date(self):

		""" Tri les alarmes par timestamp """
		
		self._manager.alarmes.sort(key=itemgetter('timestamp'))

	def _filter_by_alarm(self, data):
		""" 
			filtre les alarmes par timezone et timezone 
			param sid: l'id du client
		"""
		
		_count = 0

		try:
			_query = "DELETE FROM {} WHERE tz='{}' AND timestamp='{}' AND sid='{}'".format(self._table, data['tz'], data['timestamp'], data['sid'])
			_count = self._db.execute_query(_query)
			if _count and _count > 0:
				return data
			else:
				self._raise_on_error('No data found on db')
		except Exception as err:
			self._printer('An error occured on _filter_by_alarm - {}'.format(err))
			return False
			

	def _filter_by_sid(self, sid):
		""" 
			filtre les alarmes par sid
			param sid: l'id du client
		"""

		_count = 0

		try:
			_query = "DELETE FROM {} WHERE sid='{}'".format(self._table, sid)
			self._db.execute_query(_query)
		except Exception as err:
			self._printer('An error occured on _filter_by_alarm - {}'.format(err))


	def on_connect(self):
		""" Ecoute sur l'event connect - ajoute le client a une room unique et lance la thread principale si elle n'est pas active"""
		
		try:

			join_room(request.sid)

			with thread_lock:
				if self._manager.bgthread is None:
					self._manager.isthreadOn	= True
					self._manager.bgthread		= self._socketio.start_background_task(self._manager.on_background, self._emit_timestamp)

			self._printer('Client {} is connected'.format(request.sid))
		except Exception as err:
			self._printer('An error occured on on_connect - {}'.format(err))

	def on_disconnect(self):

		""" Ecoute sur l'event disconnect et supprime les alarmes du clients """
		
		_sid = request.sid
		self._manager.pool.spawn_n(self._filter_by_sid, _sid)
		self._printer('Client {} has been disconnected'.format(_sid))

	def on_timezoneData(self):

		""" Ecoute sur l'event timezoneData pour recuperer toute les timezones disponibles depuis un fichier json """
		
		_response = []

		try:
			with open('tools/timezones.json') as json_fd:
				_response = json.load(json_fd)
		except Exception as err:
			self._printer('An error occured while getting timezones.json - {}'.format(err))

		self._emit(event='timezoneData', data=_response)

	def _set_alarme_handler(self, data):

		""" callback de l'event on_set_alarme """
		
		try:
			if isinstance(data, dict) and \
				data.get('tz') in self._manager._timezones and \
					isinstance(data.get('timestamp'), int) and data['timestamp'] > self._get_timestamp_by_timezone(data['tz']):

						count = self._db.insert_into(self._table, data)
						if count and count > 0:
							return data
						else:
							self._raise_on_error('Something goes wrong with the insertion on_set_alarm')

			else:
				self._raise_on_error('Something goes wrong with the data emit on_set_alarm')
		except Exception as err:
			self._printer('An error occured on_set_alarme - {}'.format(err))
			return False

	def _set_alarme_callback(self, gt, *args, **kwargs):

		""" Callback de suppresion de l'alarme permettant de valider l'action
			param gt: l'objet greenthread du spawn permettant de recuperer la retour
			type gt: GreenThread
		"""
		
		_data = gt.wait()
		if _data is not False:
			self._manager.pool.spawn_n(self._emit, {'success': True, 'data':_data}, event='alarme_set')
			self._printer('An alarm has been removed')

	def on_set_alarme(self, data):

		""" Ecoute sur l'event set_alarme pour parametrer une alarme
			param data: contient les elements necessaire a la l'ajout
			type data: dict
		"""
		try:

			data['sid'] = request.sid

			_r = self._manager.pool.spawn(self._set_alarme_handler, data)
			_r.link(self._set_alarme_callback)
		
		except Exception as err:
			self._printer('An error occured on_set_alarme - {}'.format(err))

	def _remove_alarme_callback(self, gt, *args, **kwargs):

		""" Callback de suppresion de l'alarme permettant de valider l'action
			param gt: l'objet greenthread du spawn permettant de recuperer la retour
			type gt: GreenThread
		"""
		
		_data = gt.wait()
		if _data is not False:
			self._manager.pool.spawn_n(self._emit, {'success': True, 'data':_data}, event='remove_alarme')
			self._printer('An alarm has been removed')


	def on_remove_alarme(self, data):

		""" Ecoute sur l'event remove_alarme pour supprimer une alarme
			param data: contient les elements necessaire a la suppression
			type data: dict
		"""
		try:
			if isinstance(data.get('tz'), str) and isinstance(data.get('timestamp'), int):
				
				data['sid'] = request.sid

				_r = self._manager.pool.spawn(self._filter_by_alarm, data)
				_r.link(self._remove_alarme_callback)
		except Exception as err:
			self._printer('An error occured on_remove_alarme - {}'.format(err))

	def on_join(self, data):

		""" Ecoute sur l'event join pour rejoindre une room
			param data: contient la room a rejoindre
			type data: dict
		"""

		try:
			_room = data.get('room')
			if _room and isinstance(_room, str):
				self._printer('joining room {} - {}'.format(_room, request.sid), level='debug')
				join_room(_room)
				self._emit_timestamp(_room)
		except Exception as err:
			self._printer('An error occured on_join - {}'.format(err))

	def on_leave(self, data):

		""" Ecoute sur l'event leave pour quitter une room
			param data: contient la room a quitter
			type data: dict
		"""
		try:
			_room = data.get('room')
			if _room and isinstance(_room, str):
				self._printer('leaving room {} - {}'.format(_room, request.sid), level='debug')
				leave_room(_room)
		except Exception as err:
			self._printer('An error occured on_leave - {}'.format(err))
