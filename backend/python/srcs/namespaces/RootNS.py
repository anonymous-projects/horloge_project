# -*- coding: utf-8 -*-

from flask 			import request
from flask_socketio import Namespace, emit, join_room, leave_room, disconnect
from operator		import itemgetter
from threading 		import Lock

import arrow, json, sqlite3
from datetime import datetime

import os

thread_lock = Lock()

class RootNS(Namespace):

	@classmethod
	def __initspec__(cls, manager):

		""" Initialisation speciale de la class RootNs pour pouvoir communiquer avec le parent """
		
		cls._manager = manager
		cls._socketio = manager.socketio
		cls._printer = manager.printer
		cls._offset  = os.environ.get('UTC_OFFSET', -1)
		return cls

	def _emit(self, data={}, event='message', room=None):

		""" Emets un message a client, une room, un event selon les parametres 
			
			param data: les message a envoyer
			type data: any, optional
			param event: l'event de transimission
			type event: str, optional
			param room: la room de transmission
			type room: str, optional

		"""
		try:
			self._socketio.emit(event, data, room=room)
		except Exception as err:
			self._printer('An error occured while emiting socketio - {}'.format(err))

	def _get_timestamp_by_timezone(self, timezone):
			return arrow.now(timezone).shift(hours=self._offset).replace(tzinfo='UTC').timestamp

	def _emit_timestamp(self, room):

		""" Callback permettant d'emettre le timestamp actuelle selon une timezone a la room correspondant a ce timezone 
			param room: la room de transimission
			type room: str
		"""

		try:

			_currts = self._get_timestamp_by_timezone(room)
			
			self._emit(event='timestamp', data=_currts, room=room)

			for alarm in self._manager.alarmes[:]:
				
				_alarmtz = alarm.get('tz')
				_alarmts = alarm.get('timestamp')
				_sid	 = alarm.get('sid')

				if _alarmts <= _currts and room == _alarmtz:
					self._manager.alarmes.remove(alarm)
					self._emit(event='alarmRing', data=alarm, room=_sid)
				elif _alarmts > _currts + 1000:
					break
		except Exception as err:
			self._printer('An error occured on _emit_timestamp - {}'.format(err))


	def _sort_by_date(self):

		""" Tri les alarmes par timestamp """
		
		self._manager.alarmes.sort(key=itemgetter('timestamp'))

	def _filter_by_alarm(self, data):
		""" 
			filtre les alarmes par timezone et timezone 
			param sid: l'id du client
		"""
		try:
			self._manager.alarmes = [ alarm for alarm in self._manager.alarmes if not ( alarm['sid'] == data['sid'] and alarm['tz'] == data['tz'] and alarm['timestamp'] == data['timestamp'] ) ]
		except Exception as err:
			return False
		return data

	def _filter_by_sid(self, sid):
		""" 
			filtre les alarmes par sid
			param sid: l'id du client
		"""
		self._manager.alarmes = [ alarm for alarm in self._manager.alarmes if not alarm['sid'] == sid  ]


	def on_connect(self):
		""" Ecoute sur l'event connect - ajoute le client a une room unique et lance la thread principale si elle n'est pas active"""
		
		try:

			join_room(request.sid)

			with thread_lock:
				if self._manager.bgthread is None:
					self._manager.isthreadOn	= True
					self._manager.bgthread		= self._socketio.start_background_task(self._manager.on_background, self._emit_timestamp)

			self._printer('Client {} is connected'.format(request.sid))
		except Exception as err:
			self._printer('An error occured on on_connect - {}'.format(err))

	def on_disconnect(self):

		""" Ecoute sur l'event disconnect et supprime les alarmes du clients """
		
		_sid = request.sid
		self._manager.pool.spawn_n(self._filter_by_sid, _sid)
		self._printer('Client {} has been disconnected'.format(_sid))

	def on_timezoneData(self):

		""" Ecoute sur l'event timezoneData pour recuperer toute les timezones disponibles depuis un fichier json """
		
		_response = []

		try:
			with open('tools/timezones.json') as json_fd:
				_response = json.load(json_fd)
		except Exception as err:
			self._printer('An error occured while getting timezones.json - {}'.format(err))

		self._emit(event='timezoneData', data=_response)

	def on_set_alarme(self, data):

		""" Ecoute sur l'event set_alarme pour parametrer une alarme
			param data: contient les elements necessaire a la l'ajout
			type data: dict
		"""
		try:
			if isinstance(data, dict) and \
				data.get('tz') in self._manager._timezones and \
					isinstance(data.get('timestamp'), int) and data['timestamp'] > self._get_timestamp_by_timezone(data['tz']):

						data['sid'] = request.sid

						if data not in self._manager.alarmes:
							self._manager.alarmes.append(data)
							self._manager.pool.spawn_n(self._sort_by_date)
							self._manager.pool.spawn_n(self._emit, {'success': True, 'data':data}, event='alarme_set')
							self._printer('New alarm has been set')

			else:
				raise Exception('Something goes wrong with the data emit on_set_alarm')
		except Exception as err:
			self._printer('An error occured on_set_alarme - {}'.format(err))
			self._manager.pool.spawn_n(self._emit, {'success': False, 'msg':'wrong date / informations sent to server - {}'.format(err)}, event='alarme_set')

	def _remove_alarme_callback(self, gt, *args, **kwargs):

		""" Callback de suppresion de l'alarme permettant de valider l'action
			param gt: l'objet greenthread du spawn permettant de recuperer la retour
			type gt: GreenThread
		"""
		
		_data = gt.wait()
		if _data is not False:
			self._manager.pool.spawn_n(self._emit, {'success': True, 'data':_data}, event='remove_alarme')
			self._printer('An alarm has been removed')


	def on_remove_alarme(self, data):

		""" Ecoute sur l'event remove_alarme pour supprimer une alarme
			param data: contient les elements necessaire a la suppression
			type data: dict
		"""
		try:
			if isinstance(data.get('tz'), str) and isinstance(data.get('timestamp'), int):
				
				data['sid'] = request.sid

				_r = self._manager.pool.spawn(self._filter_by_alarm, data)
				_r.link(self._remove_alarme_callback)
		except Exception as err:
			self._printer('An error occured on_remove_alarme - {}'.format(err))

	def on_join(self, data):

		""" Ecoute sur l'event join pour rejoindre une room
			param data: contient la room a rejoindre
			type data: dict
		"""

		try:
			_room = data.get('room')
			if _room and isinstance(_room, str):
				self._printer('joining room {} - {}'.format(_room, request.sid), level='debug')
				join_room(_room)
				self._emit_timestamp(_room)
		except Exception as err:
			self._printer('An error occured on_join - {}'.format(err))

	def on_leave(self, data):

		""" Ecoute sur l'event leave pour quitter une room
			param data: contient la room a quitter
			type data: dict
		"""
		try:
			_room = data.get('room')
			if _room and isinstance(_room, str):
				self._printer('leaving room {} - {}'.format(_room, request.sid), level='debug')
				leave_room(_room)
		except Exception as err:
			self._printer('An error occured on_leave - {}'.format(err))
