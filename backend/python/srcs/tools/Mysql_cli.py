# -*- coding: utf-8 -*-
import mysql.connector

class Mysql_client(object):

	def __init__(self, dbconn=None):
		self.conn = mysql.connector.connect(**dbconn)
		self._retry = 0
		print("Mysql_client: connection etablished")
		# self.conn.disconnect()

	def close(self):
		self.conn.close()

	def commit_query(self):
		self.conn.commit()

	def fetchall(self, curs):
		return curs.fetchall()

	def fetchone(self, curs):
		return curs.fetchone()

	def execute_query(self, query, values=tuple(), fetch=None, commit=True, dictionary=True):
		try:

			curs = self.conn.cursor(dictionary=dictionary)
			curs.execute(query, tuple(values))
			
			self._retry = 0

			if fetch != None:
				res = getattr(self, 'fetch'+fetch)(curs)
				curs.close()
				return res

			if commit == True:
				self.commit_query()

			count = curs.rowcount
			curs.close()
			return count

		except mysql.connector.errors.OperationalError as err:

			if self._retry < 4:
				print('Reconnection - error -', err)
				# self.conn.rollback()
				self.conn.reconnect()
				self._retry += 1
				return self.execute_query(query, values, fetch, commit, dictionary)
		except Exception as err:
			print(err)

	def execute_many_query(self, query, values=tuple(), commit=True):

		try:

			curs = self.conn.cursor()
			curs.executemany(query, values)

			self._retry = 0

			if commit == True:
				self.commit_query()

			count = curs.rowcount
			curs.close()
			return count

		except mysql.connector.errors.OperationalError as err:

			if self._retry < 4:
				print('Reconnection - error -', err)
				# self.conn.rollback()
				self.conn.reconnect()
				self._retry += 1
				return self.execute_many_query(query, values, commit)
	
	def insert_into_if_not_exists(self, table, row_to_insert, row_to_check, table_to_check=None, commit=True, print_query=None):
		row_insert	= self._split_4_insert(row_to_insert)
		row_check	= self._split_4_insert(row_to_check)
		rows_values		= row_insert['values']+row_check['values']
		query 		= """
			INSERT INTO `{table}` ({columns_insert})
			SELECT * FROM (SELECT {values_insert}) AS tmp
			WHERE NOT EXISTS (
		    	SELECT {values_check} FROM `{table_to_check}` WHERE {condition_check}
			)
		""".format(
			table			= table,
			table_to_check  = table_to_check or table,
			columns_insert	= ",".join(row_insert['keys']),
			values_insert	= row_insert['placeholder'],
			values_check	= ",".join(row_check['keys']),
			condition_check = " AND ".join([k+"=%s"for k,v in row_to_check.items()])
		)
		if print_query == True:
			print(query)
		return self.execute_query(query, rows_values, commit=commit)


	def insert_into(self, table, row_to_insert, commit=True, print_query=None):
		row_len     = len(row_to_insert)
		row_keys    = row_to_insert.keys()
		row_values  = row_to_insert.values()
		placeholder = ", ".join(["%s"] * row_len)
		query = """INSERT INTO `{table}` ({columns}) VALUES ({values})""".format(
			table=table,
			columns=",".join(row_keys),
			values=placeholder
		)
		if print_query == True:
			print(query)
		return self.execute_query(query, row_values, commit=commit)

	def insert_many_into(self, table, row_to_inserts, commit=True, print_query=None):
		
		if not row_to_inserts:
			return
		
		row_len     = len(row_to_inserts[0])
		row_keys    = row_to_inserts[0].keys()
		placeholder = ", ".join(["%s"] * row_len)
		
		query = """INSERT INTO {table} ({columns}) VALUES ({values})""".format(
			table=table,
			columns=",".join(row_keys),
			values=placeholder
		)
		if print_query == True:
			print(query, len(row_to_inserts))
		
		row_values = [tuple(row_to_insert.values()) for row_to_insert in row_to_inserts]
		
		return self.execute_many_query(query, row_values, commit=commit)

	def delete_many(self, table, column, row_to_delete, print_query=None):
		row_len		= len(row_to_delete)
		placeholder = ", ".join(["%s"] * row_len)
		query = """DELETE FROM {table} WHERE {column} IN ({values})""".format(
			table=table,
			column=column,
			values=placeholder
		)
		if print_query == True:
			print(query)
		return self.execute_query(query, row_to_delete)

	def last_insert_id(self, commit=False):
		last = self.execute_query("SELECT LAST_INSERT_ID() as id", fetch='one', commit=commit)
		return last['id']
	
	def _split_4_insert(self, row):
		row_len = len(row)
		return {
			'len': row_len,
			'keys': row.keys(),
			'values':list(row.values()),
			'placeholder':", ".join(["%s"] * row_len)
		}
