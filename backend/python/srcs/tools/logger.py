import 	os, logging, time
import logging.handlers

def getLogger(
		filename=os.path.join("logs/socket_serv.log"), 
		level="INFO", 
		fmt='[%(asctime)s]:[%(module)s][%(levelname)s]%(message)s',
		loggername='socket_serv'):

	"""
		Logger for the AutoReceive, allow to centralized only the log from AutoReceive & Co

		:return: logger target
		:rtype: object
	"""
	try:
		
		if not os.path.isdir('logs'):
			os.mkdir('logs')

		logger = logging.getLogger(loggername)
		if not logger.handlers:
			megabyte 	= 1000000
			maxBytes 	= 30*megabyte
			formatter 	= logging.Formatter(fmt, '%Y/%m/%d - %H:%M:%S')
			handler 	= logging.handlers.RotatingFileHandler(filename, mode='a', encoding="utf-8", maxBytes=maxBytes, backupCount=10)
			handler.setLevel(level)
			handler.setFormatter(formatter)
			logger.setLevel(level)
			logger.addHandler(handler)
	except Exception as err:
		print('An error occured getLogger - {}'.format(err))
	else:
		return logger
