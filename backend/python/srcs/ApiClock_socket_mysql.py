# -*- coding: utf-8 -*-

import logging
import threading
import os, json, time

import eventlet

eventlet.monkey_patch()

from flask 			import Flask, session, request
from flask_socketio import SocketIO, Namespace, send, emit, join_room, leave_room, close_room, rooms, disconnect

from tools 		import getLogger, Mysql_client
from datetime	import datetime
from namespaces import RootNS_mysql

async_mode	= 'eventlet'
app			= Flask(__name__)

class ManagerSocketNS(object):

	def __init__(self, port):

		"""Initialisation de la class ManagerSocketNS / Creation du logger et recuperation des timezones

		:param port: le port d'ecoute de la socket
		:type port: int
		
		"""
		self._logger = getLogger()
		self._db_connection()

		self.port			= port
		self.bgthread		= None
		self.isconnected	= None
		self.alarmes		= []

		self._get_timezones()

		self.pool 			= eventlet.GreenPool(size=len(self._timezones)*5)
		self.socketio		= SocketIO(app, async_mode=async_mode, cors_allowed_origins="*")

		_rootNSInitiated	= RootNS_mysql.__initspec__(self)
		_rns 				= _rootNSInitiated('/')

		self.socketio.on_namespace(_rns)

		self.printer('\nSocketServer init')

	def _db_connection(self):
		
		""" connection a la base de donnee SQL"""

		with open("params/db_infos.json") as dbinfo:
			_dbinfo = json.load(dbinfo)
			self.db = Mysql_client(_dbinfo)


	def _get_timezones(self):
		
		""" Set les timezones dans un frozenset depuis un fichier json """

		try:
			with open('tools/timezones.json') as json_fd:
				_timezones = json.load(json_fd)
		except Exception as err:
			self._timezones = tuple()
			self.printer('Error while loading timezones.json', level='error')
		else:
			self._timezones = frozenset( (zone.get('value') for continent in _timezones for zone in continent.get('zones', [])) )


	def printer(self, msg, level='info'):
		getattr(self._logger, level)(u"{}".format(msg))

	def on_background(self, callback):

		""" Thread principale du serveur socket permettant de dispatcher les timezones a chaque seconde sur les differentes rooms 
			param callback: la callback d'emission du timestamp actuelle
		"""
	
		self.printer('Start remote scanner thread', level='debug')

		while self.isthreadOn:
			self.socketio.sleep(1)
			self.pool.imap(callback, self._timezones)
			
		self.printer('Background thread Stopped', level='debug')

	def start(self):

		""" Mise en route du serveur socket """
		
		self.printer('SocketServer start - waiting for client')
		self.socketio.run(app, host='0.0.0.0', port=self.port, debug=True)

if __name__ == "__main__":

	try:
		_M = ManagerSocketNS(os.environ.get('PORT', 5020))
		_M.start()
	except Exception as err:
		print('serveur python an error occured - {}'.format(err))
