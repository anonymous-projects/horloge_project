import React, {useReducer} from 'react'

export const Store = React.createContext();

const initialState = {
  timezone: 'Europe/Paris',
  alarmes:{}
};

function has_key(alarmState, key) {
	if (alarmState.hasOwnProperty(key)){
		return alarmState[key]
	}
	return []
}

function reducer(state, action) {
  switch (action.type) {
    case 'set_tz':
    	return { ...state, timezone: action.tz };
    case 'add_alarme':
    	return { ...state, alarmes:{...state.alarmes, [action.tz]:[...has_key(state.alarmes, action.tz), action.timestamp]}}
    case 'remove_alarme':
    	return { ...state, alarmes:{...state.alarmes, [action.tz]:state.alarmes[action.tz].filter(timestamp => timestamp !== action.timestamp)}}
    case 'reset_alarme':
      return { ...state, alarmes:{}}
    default:
    	return state;
  }
}

export function StoreProvider(props) {
	const [state, dispatch] = useReducer(reducer, initialState);
	const value = { state, dispatch };

	return <Store.Provider value={value}>{props.children}</Store.Provider>
}

// {
// 	'/America/Boston':[1248421891, 1819121, 181515121]
// }

//failed: Error during WebSocket handshake: net::ERR_CONNECTION_RESET