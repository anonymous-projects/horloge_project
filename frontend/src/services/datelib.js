const currentDate = (_date) => {

    const date = _date.getDate(); 
    let month = _date.getMonth() + 1; 

    if (month < 10) {
      month = '0' + month
    }

    const year = _date.getFullYear()
    const hours = _date.getHours()
    const min = _date.getMinutes()
    const sec = _date.getSeconds()
    return `${year}-${month}-${date}T${hours}:${min}`
}

const addZero = (curr, min) => {
    if (curr < min){
        curr = '0' + curr
    }
    return curr
}

const getHours = (date) => {

    const hours = addZero(date.getHours(), 10)
    const min   = addZero(date.getMinutes(), 10)
    return `${hours}:${min}`
}

function isNumeric(num){
  return !isNaN(num)
}

const dateformating = (timestamp) => {

    let _date 

    if(isNumeric(timestamp))
        _date = new Date(timestamp*1000)
    else
        _date = new Date(timestamp)

    const day   = _date.getDay() 
    const date  = _date.getDate()
    const month = _date.getMonth() 
    const year  = _date.getFullYear()
    
    let hours = addZero(_date.getHours(), 10)
    let min   = addZero(_date.getMinutes(), 10)
    let sec   = addZero(_date.getSeconds(), 10)

    const daysOfWeek = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]
    const monthNames = ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembere"]

    return `${daysOfWeek[day]} ${date} ${monthNames[month]} ${year} ${hours}:${min}:${sec}`
}

export const datelib = {
    currentDate,
    dateformating,
    getHours
};