import React from 'react';
import { useState, useEffect } from 'react';

import Typography from '@material-ui/core/Typography';

import { Store } from '../helpers/Store';
import {datelib} from '../services/datelib'

import grey from '@material-ui/core/colors/grey';
import green from '@material-ui/core/colors/green';

/**
 * Ce component permet d'afficher l'heure provenant de la socket ainsi que sa timezone,
 * et de rejoindre dynamiquement la room correspondant au timezone set dans le component TimeZoneSelector
 * Il est updaté si lùun des objet suivant est modifié : socket, state.timezone, reconnection
 */

export default function ClockHandler({socket}) {

  const { state } = React.useContext(Store);
  const [date, setDate] = React.useState(null);
  const [reconnection, setReconnection] = React.useState(0);

  useEffect(() => {
    
    console.log('useEffect onMount Listening')

    socket.on('timestamp', timestamp => {
      setDate(timestamp)
      // console.log(timestamp)
    });

    socket.on('reconnect', () => {
      setReconnection(prevState => prevState + 1)
      console.log("client reconnection from server");
    })
    
    return () => {
      socket.removeAllListeners("timestamp");
      socket.removeAllListeners("reconnect");
    }

  }, [socket]);

  useEffect(() => {
    
    console.log('joining room', state.timezone)
    socket.emit('join', {room: state.timezone});

    return () => {
      console.log('leaving room', state.timezone)
      socket.emit('leave', {room: state.timezone})
    }
  }, [socket, state.timezone, reconnection]);

  return (
    <div>
      <Typography align="center" variant="h4" gutterBottom>{date ? datelib.dateformating(date) : 'Connection en cours'}</Typography>
      <Typography align="center" variant="subtitle1" style={{color:grey[600]}}>{state.timezone}</Typography>
    </div>
  )
}
