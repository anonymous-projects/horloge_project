import React, { useState, useEffect } from 'react';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import { Store } from '../helpers/Store';
import {setTimezone} from '../actions/timezone.js'


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 200,
      },
    },
  }),
);

/**
 * Ce component permet de selectioner une timezone, a partir d'une liste recuperer a sa creation via la socket
 */

export default function TimeZoneSelector({socket}) {
  
  const classes = useStyles();
  
  const { state, dispatch } = React.useContext(Store);
  const [timezones, setTimezones] = React.useState([]);

  const { alarmes } = state

  const handleChange = (event) => {
    dispatch(setTimezone(event.target.value));
  };

  React.useEffect(() => {
    
    socket.once('timezoneData', timezones => {
      setTimezones(timezones)
    });
    
    socket.emit('timezoneData');

    return () => {
      setTimezones([])
    }
  }, [socket]);

  return (
      <FormControl className={classes.root}>
        <Select native input={<Input id="grouped-native-select" />} onChange={handleChange}>
          <option value="">Selectionner une timezone</option>
           {timezones.map(
            tzgrp => <optgroup key={tzgrp.group} label={tzgrp.group} children={tzgrp.zones.map(
              tz => <option style={{backgroundColor:alarmes.hasOwnProperty(tz.value) && alarmes[tz.value].length > 0 ? "grey" : "white"}} key={tz.name} value={tz.value} >{tz.name}</option>)}/>
           )}
        </Select>
      </FormControl>
  );
}
