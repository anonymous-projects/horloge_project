import React, { useState, useEffect } from 'react';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import io from 'socket.io-client';

import AlarmeSetter     from './Alarme_setter'
import AlarmesDisplay  from './Alarmes_display'

import ClockHandler    from './Clock_handler'
import TimeZoneSelector from './Timezone_selector'

import { Store } from '../helpers/Store';
import { removeAlarmes, resetAlarmes } from '../actions/alarmes.js'

import { withSnackbar } from 'notistack';

console.log(process.env.REACT_APP_SOCKETPORT)

const SOCKETADDR = process.env.hasOwnProperty('REACT_APP_SOCKETADDR') ? process.env.REACT_APP_SOCKETADDR : 'localhost'
const SOCKETPORT = process.env.hasOwnProperty('REACT_APP_SOCKETPORT') ? process.env.REACT_APP_SOCKETPORT : 5020

const socket = io(`${SOCKETADDR}:${SOCKETPORT}`);

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minHeight:'100vh',
    },
    addBottom:{
      marginBottom:theme.spacing(10)
    },
    clock:{
      marginBottom:theme.spacing(15)
    }
  }),
);

function ClockSocket({enqueueSnackbar}) {

  const classes = useStyles();
  const { state, dispatch } = React.useContext(Store);

  useEffect(() => {

    socket.on("connect", () => {
      console.log("client connected to server", socket.id);
    });

    socket.on("disconnect", () => {
      console.log("client disconnected from server");
      dispatch(resetAlarmes())

    });

    socket.on("alarmRing", response => {
      enqueueSnackbar(`Alarm ! ${response.tz}`, { variant: 'success'})
      dispatch(removeAlarmes({tz:response.tz, timestamp:response.timestamp}))
    });


    return () => {
      socket.removeAllListeners("alarmRing");
      socket.removeAllListeners("connect");
      socket.removeAllListeners("disconnect");
    }

  }, [enqueueSnackbar, dispatch]);

  return (
    <Grid container justify='center' alignItems='center' direction='column' className={classes.root}>
      <Grid item className={classes.clock}>
        <ClockHandler socket={socket}/>
      </Grid>
      <Grid item className={classes.addBottom}>
        <TimeZoneSelector socket={socket}/>
      </Grid>
      <Grid item className={classes.addBottom}>
        <AlarmeSetter socket={socket}/>
      </Grid>
      <Grid item>
        <AlarmesDisplay socket={socket}/>
      </Grid>
    </Grid>
  );

}

export default withSnackbar(ClockSocket);
