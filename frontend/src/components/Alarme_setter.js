import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import {datelib} from '../services/datelib'

import AlarmIcon from '@material-ui/icons/Alarm';
import AddAlarmIcon from '@material-ui/icons/AddAlarm';
import IconButton from '@material-ui/core/IconButton';

import { Store } from '../helpers/Store';
import { setAlarmes } from '../actions/alarmes.js'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root:{
      backgroundColor:'yellow'
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent:'center',
      alignItems:'center'
    },
    date: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
      "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
            display: "none"
     }
    },
    clock: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
      "& input::-webkit-clear-button": {
            display: "none"
     }
    }

  }),
);

/**
 * Ce component permet de creer une alarmes et de l'emettre au serveur socket
 */

export default function AlarmeSetter({socket}) {
  const classes  = useStyles();

  const { state, dispatch } = React.useContext(Store);
  const [ alarmDatetime, setAlarmDatetime ] = React.useState(new Date().toISOString().split('.')[0]);

  React.useEffect(() => {
    
    socket.on('alarme_set', response => {

      if (response.success == true) {
        dispatch(setAlarmes(response.data))
      }
      else if (response.msg){
        console.log(response.msg)
      }
    });

    return () => {
      socket.removeAllListeners("alarme_set");
    }

  }, [socket]);

  const handleChange = (event) => {
    const id = event.target.id
    const value = event.target.value
    setAlarmDatetime(value)
  }

  const onBtnClick = (e) => {

    if (alarmDatetime != null) {

      const _timestamp = new Date(alarmDatetime).getTime()/1000
      socket.emit('set_alarme', {tz:state.timezone, timestamp:_timestamp});
    
    }

  }

  return (
    <Grid container alignItems='center'>
      <Grid item>
        <form className={classes.container} noValidate>
          <TextField
            id="datetime-local"
            label="Alarmes"
            type="datetime-local"
            defaultValue={new Date().toISOString().split('.')[0]}
            onChange={handleChange}
            className={classes.clock}
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              /*min:new Date().toISOString().split('.')[0]+'.00'*/
            }}
          />
        </form>
      </Grid>
      <Grid item>
        <IconButton color="primary" aria-label="add an alarm" onClick={(e)=>{onBtnClick(e)}}>
          <AddAlarmIcon style={{fontSize:48}}/>
        </IconButton>
      </Grid>
    </Grid>
  );
}
