import React, { useState, useEffect } from 'react';

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import AlarmIcon from '@material-ui/icons/Alarm';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { Store } from '../helpers/Store';
import { datelib } from '../services/datelib'
import { removeAlarmes } from '../actions/alarmes.js'

import green from '@material-ui/core/colors/green';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(1),
      width:"90vw"
    },
  }),
);

/**
 * Ce component permet d'afficher les Alarmes de la timezone selectionnée.
 */

export default function AlarmesDisplay({socket}) {

  const classes = useStyles();
  const { state, dispatch } = React.useContext(Store);
  const { alarmes } = state

  const onBtnClick = (event, timestamp) => {
  	socket.once('remove_alarme', response => {
	    if (response.success == true) {
	      dispatch(removeAlarmes({tz:state.timezone, timestamp:timestamp}))
	    }
    });
    socket.emit('remove_alarme', {tz:state.timezone, timestamp:timestamp});
  }

  const alarme_btn = (timestamp, idx) => {
  	return(
  		<Button
  			id='idd'
	        variant="contained"
	        color= {idx % 2 == 0 ? "primary" : "secondary"}
	        className={classes.button}
	        startIcon={<AlarmIcon />}
	        onClick={(e) => {onBtnClick(e, timestamp)} }
	      >
	        {datelib.dateformating(timestamp)}
	    </Button>
     )
  }

  const timezone_typo = (tz) => {
  	return (
  		<Typography align="center" variant="h6" gutterBottom> {tz} </Typography>
  	)
  }

  return (
  	<Grid container justify='center' alignItems='center' direction="column">
  		{alarmes.hasOwnProperty(state.timezone) ? alarmes[state.timezone].map((timestamp, idx) => <Grid item key={idx}> {alarme_btn(timestamp, idx)} </Grid>) : null}
  	</Grid>
  )

}
