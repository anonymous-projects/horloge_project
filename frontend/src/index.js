import React from 'react';
import { render } from 'react-dom'

import { StoreProvider } from './helpers/Store';
import App from './components/App';
import { SnackbarProvider } from 'notistack';

render(
	<StoreProvider>
		<SnackbarProvider maxSnack={5}>
			<App />
		</SnackbarProvider>
	</StoreProvider>,
	document.getElementById('root')
);
