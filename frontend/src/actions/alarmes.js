export const setAlarmes = action => ({
  type: 'add_alarme',
  tz: action.tz,
  timestamp:action.timestamp
})

export const removeAlarmes = action => ({
  type: 'remove_alarme',
  tz: action.tz,
  timestamp:action.timestamp
})

export const resetAlarmes = action => ({
  type: 'reset_alarme'
})